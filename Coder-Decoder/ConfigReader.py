from abc import ABC, abstractmethod
from ConfigException import Config_Exception
from ConfigReaderInterface import ConfigReaderInterface
import re


config_exception = Config_Exception()

class ConfigReader(ConfigReaderInterface):

    def read_config(self, config_file_name: str) -> dict:
        keys = {self.buffer_size_param_name, self.file_name_for_coder_param_name, self.coder_run_option_param_name}
        config_dict = {}
        try:
       
            with open(config_file_name, "r") as f:
                lines = f.readlines()
        
                if len(lines) < 3: raise Config_Exception('There are missing parameters')
            
                if len(lines) > 3: raise Config_Exception('There are too much parameters')

                for line in lines:
                    key, value = line.split(" " + self._param_delimiter + " ")
                    value = re.sub(r"\n", "", value)
                    if key in keys:
                        keys.remove(key)
                        if key == self.coder_run_option_param_name and not (value == "code" or value == "decode"):
                            raise Config_Exception('Only modes are "code" and "decode"!')
                        elif key == self.buffer_size_param_name:
                            value = int(value)
                        config_dict[key] = value

                    else:
                        raise Config_Exception("Unknown parameter: {}".format(key) )


                # self._config_dict[self.buffer_size_param_name] = int(lines[0][13:])
                # bs = int(lines[0][13:])
                # print(bs)

                # self._config_dict[self.file_name_for_coder_param_name] = lines[1][12:-1]
                # fn = lines[1][12:-1]
                # print(fn)

                # self._config_dict[self.coder_run_option_param_name] = lines[2][14:]
              

        #except TypeError:
        #     raise Config_Exception('Amount of byte should be int type!')

        except FileNotFoundError:
            raise Config_Exception('Given directory is wrong!')


        return config_dict



    

