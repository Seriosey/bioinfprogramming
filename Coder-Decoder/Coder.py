from abc import ABC, abstractmethod
from CoderInterface import CoderInterface



class Coder(CoderInterface):
    def run(self, coder_info: str, string_to_process: str) -> str:
        if coder_info == "code":
            return self._code(string_to_process)
        else:
            return self._decode(string_to_process)

        pass

    def _code(self, string_to_code: str) -> str:
        return string_to_code[::-1]

    def _decode(self, string_to_decode: str):
        return string_to_decode[::-1]
