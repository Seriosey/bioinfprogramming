from abc import ABC, abstractmethod
from contextlib import contextmanager
from FileReaderInterface import FileReaderInterface


class FileReader(FileReaderInterface):
    @contextmanager
    def read_file(self, file_name: str, buffer_size: int):
       try:
           print(file_name)
           f = open(file_name, "r")
           chunks = []
           chunk = f.read(buffer_size)
           while len(chunk) > 0:
               chunks.append(chunk)
               chunk = f.read(buffer_size)
           yield chunks
       except FileNotFoundError:
           print("File not found")
       finally:
           f.close(
        )

