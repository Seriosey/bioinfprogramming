import blosum as bl
import pandas as pd
import numpy as np
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Viterbi import trace_back
from ViterbiRecounting import f, fx, fy

# x = 'VGAHAGEYGA'
# y = 'VNVEEAGG'
d = 2
blos = bl.BLOSUM(50) 

p = {'A' : {'A' : 0.5, 'G' : 0.05, 'T' : 0.3, 'C' : 0.15},
     'G' : {'A' : 0.05, 'G' : 0.5, 'T' : 0.15, 'C' : 0.3},
     'T' : {'A' : 0.3, 'G' : 0.15, 'T' : 0.5, 'C' : 0.05},
     'C' : {'A' : 0.15, 'G' : 0.3, 'T' : 0.05, 'C' : 0.5}
     }

p_hmm = {'P_mm': 0.5, 'P_dm': 0.8, 'P_im': 0.8, 'P_md': 0.2, 'P_mi': 0.2, 'P_dd': 0.1, 'P_ii': 0.1}


class ForwardAlg:
    def __init__(self, seqx, seqy, d=2):
        self.x = seqx
        self.y = seqy
        self.d = d
        self.matrix = self.viterbi_matrix(self.x, self.y)
        self.path = trace_back(self.matrix, self.x, self.y)
        #self.emiss = {'A': }

    def read_sequence_from_file(self, file_path):
        with open(file_path, "r") as file:
            record = SeqIO.read(file, "fasta")
        return record.seq

    def log_add(self, x, y):
        if x < y:
            x, y = y, x
        if y == float('-inf'):
            return x
        return x + np.log1p(np.exp(y - x))

    def score(self, i, j):
        return blos[self.x[i-1]][self.y[j-1]]

    def v_mf(self):
        v =np.zeros((len(self.x)+1, len(self.y)+1))
        for i in range(len(self.x) + 1):
            for j in range(len(self.y) + 1):
                if i == 0:
                    v[i][j] =0# -2 * j
                elif j == 0:
                    v[i][j] = 0#-2 * i
                else:
                    v[i][j] = self.log_add(v[i-1][j-1] + self.score(i, j),\
                              self.log_add(v[i-1][j] - self.d, v[i][j-1] - self.d))
        return v
    
    def v_m(self, i, j):
        if i == 0 and j == 0: return 0
        elif i<1 or j<1: return 0
        else: 
            pi = self.score(i, j)
        
            return self.log_add(0.5*self.v_m(i-1, j-1)+ self.score(i, j), \
                   self.log_add(0.8*self.v_x(i-1, j-1) - self.d, \
                                0.8*self.v_y(i-1, j-1) - self.d)) #+ np.log(0.10/0.25)

    def v_x(self, i, j):
        if i == -1 or j == -1: return 0
        if i == j == 1: return 0
        else: return self.log_add(0.2*self.v_m(i-1, j), \
                                  0.1*self.v_x(i-1, j)) #+ np.log(0.10/0.25)

    def v_y(self, i, j):
        if i == -1 or j == -1: return 0
        if i == j == 1: return 0
        else: return self.log_add(0.2*self.v_m(i, j-1), \
                                  0.1*self.v_y(i, j-1)) #+ np.log(0.10/0.25)
        

    def v_m0(self, i, j):
        x = self.x
        y = self.y
        
        if i == 0 and j == 0: return 0
        elif i<1 or j<1: return 0
        else: 
            #pi = self.score(i, j)
            return np.log(p_hmm['P_mm'] * np.exp(self.v_m0(i-1, j-1)) + \
                          p_hmm['P_dm'] * np.exp(self.v_d(i-1, j-1)) + \
                          p_hmm['P_im'] * np.exp(self.v_i(i-1, j-1))) + \
                          np.log(self.emission('m', x[i-1], x, self.path)/0.25)

    def v_d(self, i, j):
        if i == 0 or j == 0: return 0
        if i == j == 1: return 0
        else: return np.log(p_hmm['P_md']*np.exp(self.v_m0(i, j-1)) + \
                            p_hmm['P_dd']*np.exp(self.v_d(i, j-1))) + \
                            np.log(self.emission('d', x[i], x, self.path)/0.25)

    def v_i(self, i, j):
        if i == 0 or j == 0: return 0
        if i == j == 1: return 0
        else: return np.log(p_hmm['P_mi']*np.exp(self.v_m0(i-1, j)) + \
                            p_hmm['P_ii']*np.exp(self.v_i(i-1, j)))  #+ np.log(self.emission('i', x[i], x, self.path)/0.25)
        

    def emission(self, state, nucleotide, x: list, path):
        c = 1
        for i in range(len(path)):
            if (path[i] == state and x[i]) == nucleotide: c+=1
        
        c /= x.count(nucleotide) + 1
        return c
    
    def estimate_op(self, paths):
        P_mm, P_xm, P_ym, P_mx, P_my, P_xx, P_yy = 0, 0, 0, 0, 0, 0, 0
        m, x, y = 1, 1, 1

        for path in paths:
            m += path.count('t')
            x += path.count('d')
            y += path.count('i')
            for k in range(len(path)-1):
                if path[k] == 't': 
                    if path[k+1] == 't': P_mm +=1
                    if (path[k+1] == 'd'): P_mx +=1
                    elif path[k+1] == 'i': P_my +=1

                elif path[k] == 'd': 
                    if path[k+1] == 'd': P_xx +=1
                    elif path[k+1] == 't': P_xm +=1
                
                elif path[k] == 'i': 
                    if path[k+1] == 'i': P_yy +=1
                    elif path[k+1] == 't': P_ym +=1

        P_mm /= m; P_xm /= x; P_ym /= y; P_mx /= m; 
        P_my /= m; P_xx /= x; P_yy /= y

        return {'P_mm': round(P_mm, 2), 'P_dm': P_xm, 'P_im': P_ym, \
                'P_md': round(P_mx, 2), 'P_mi': P_my, 'P_dd': P_xx, 'P_ii': P_yy }
    
    def viterbi_matrix(self, x, y):
        # x = self.x
        # y = self.y
        matrix = np.zeros((len(y)+1, len(x)+1, 3))

        for i in range(0, len(x)+1):
            for j in range(0, len(y)+1):
                matrix[j, i] = np.array([f(x, y, i, j), \
                                         fx(x, y, i, j), \
                                         fy(x, y, i, j)])
        return matrix
    

    def needleman_wunsch(self, x = None, y = None, result='score'):
        
        if (x and y): 
            self.x = x
            self.y = y

        v = self.v_mf()

        if result == 'alignment':
            align_seq1, align_seq2 = '', ''
            i, j = len(self.x), len(self.y)

            while i > 0 or j > 0:
                current_score = v[i, j]

                if i > 0 and v[i-1, j] + self.d == current_score:
                    align_seq1 = self.x[i-1] + align_seq1
                    align_seq2 = '-' + align_seq2
                    i -= 1
                elif j > 0 and v[i, j-1] + self.d == current_score:
                    align_seq1 = '-' + align_seq1
                    align_seq2 = self.y[j-1] + align_seq2
                    j -= 1
                else:
                    align_seq1 = self.x[i-1] + align_seq1
                    align_seq2 = self.y[j-1] + align_seq2
                    i -= 1
                    j -= 1

            # Запись выравнивания в файл
            self.write_alignment_to_file(align_seq1, align_seq2, "output_alignment.fasta")
            return align_seq1, align_seq2

        return v[-1][-1]

    def write_alignment_to_file(self, align_seq1, align_seq2, file_path):
        records = [
            SeqRecord(Seq(align_seq1), id="seq1"),
            SeqRecord(Seq(align_seq2), id="seq2")
        ]

        with open(file_path, "w") as file:
            SeqIO.write(records, file, "fasta")




x = 'TTACACCGAGT'
y = 'TAGAATAT'
a = 'T_AGA_ATA_T'
v_matrix = np.zeros((len(x)+1, len(x)+1))
alg = ForwardAlg(x, y)


for i in range(0, len(x)+1):
    for j in range(0, len(x)+1):
        v_matrix[j, i] = alg.v_m0(i, j)

print(pd.DataFrame(v_matrix))

path = 'MDMDM'

#print(alg.e_m('T', 'TTACGT', path))


#print(alg.v_mf())

#forward_algorithm('TTACGACTACTG', p, emission_probs, initial_probs)


file_path_x = "sequence_x.fasta"
file_path_y = "sequence_y.fasta"
#nw = ForwardAlg('GACGCCAAT', 'GGGGTACCG')
#result = nw.needleman_wunsch('GACGCCAAT', 'GGGGTACCG') # result='alignment'

#import pandas as pd

# print(pd.DataFrame(nw.v_mf()))
# print('\n')
#print('normal result ', result)

# result1 = (nw.needleman_wunsch('GACGCCAAT', 'GACGCCAAT'))/1 # result='alignment'
# result2 = nw.needleman_wunsch('GGGGTACCG', 'GGGGTACCG')
#print('result of same ', result1, '\n', result2)
print(alg.path)
print('параметры используемой СММ: ', p_hmm)
print('эмпирические параметры СММ: ', alg.estimate_op([alg.path]))