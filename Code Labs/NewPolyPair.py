from Bio import Align, Phylo
from Bio.Phylo.TreeConstruction import DistanceCalculator, DistanceTreeConstructor, DistanceMatrix
from Bio.Align import MultipleSeqAlignment
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq
import numpy as np
from Bio import SeqIO
import ForwardAlg as nwsh
import blosum as bl

blos = bl.BLOSUM(50)
nw = nwsh.ForwardAlg("AAAAAAA", "AAAAAAA")

def calculate_distance_bio(seq1, seq2):
    aligner = Align.PairwiseAligner()
    aligner.mode = 'global'

    # глобальное выравнивание
    alignments = aligner.align(seq1, seq2)
    # Берем первое (лучшее) выравнивание
    best_alignment = alignments[0]
    #print(best_alignment)
    # Вычисляем длину выравнивания
    alignment_length = len(best_alignment[0])

    # Вычисляем количество несовпадающих символов (различий)
    mismatch_count = sum(1 for i in range(alignment_length) if best_alignment[0][i] != best_alignment[1][i])
    # Вычисляем расстояние как доля несовпадающих символов от общей длины выравнивания
    distance = mismatch_count / alignment_length

    return distance

def calculate_distance(seq1, seq2):
    S_max = (nw.needleman_wunsch(seq1, seq1) + nw.needleman_wunsch(seq2, seq2))/2
    #print('Smax ', S_max)
    p_match = 1 / len(blos)  # Вероятность совпадения
    p_mismatch = 1 - p_match  # Вероятность различия
    S_rand = calculate_S_rand(seq1, seq2)
    #print('Srand ', S_rand)

    alignment_score = nw.needleman_wunsch(seq1, seq2)
    #print('alignment score  ', alignment_score)
    distance = -np.log(abs((alignment_score - S_rand) / (S_max - S_rand)))
    #print('distance ', distance)
    return distance

def calculate_S_rand(seq1, seq2, d=2):
    # Рассчитываем частоты появления остатков в последовательностях
    residues_count_seq1 = {residue: seq1.count(residue) for residue in set(seq1)}
    residues_count_seq2 = {residue: seq2.count(residue) for residue in set(seq2)}

    # Рассчитываем S_rand через двойную сумму
    S_rand = 0
    L = len(seq1)  # Используем длину одной из последовательностей

    for residue1 in residues_count_seq1:
        for residue2 in residues_count_seq2:
            # Рассчитываем счет подстановки для пары остатков
            substitution_score = blos[residue1][residue2]
            S_rand += substitution_score * residues_count_seq1[residue1] * residues_count_seq2[residue2] * d

    return S_rand / L




def read_sequences_from_fasta(file_path):
    sequences = []
    with open(file_path, 'r') as fasta_file:
        for record in SeqIO.parse(fasta_file, 'fasta'):
            sequences.append(record)
    return sequences



def fitch_margoliash(tree, sequences):
    def fitch_margoliash_recur(node):
        if node.is_leaf():
            return {node.name: set(node.alignment)}

        left_states = fitch_margoliash_recur(node.left)
        right_states = fitch_margoliash_recur(node.right)

        common_states = left_states.keys() & right_states.keys()

        node_states = {}

        for state in common_states:
            node_states[state] = left_states[state] & right_states[state]

        for state in left_states.keys() - right_states.keys():
            node_states[state] = left_states[state]

        for state in right_states.keys() - left_states.keys():
            node_states[state] = right_states[state]

        return node_states

    alignment_states = fitch_margoliash_recur(tree)

    # Choose the consensus state for each site
    consensus_alignment = []
    for site in zip(*sequences):
        site_states = [alignment_states[state] for state in site]
        consensus_state = set.intersection(*site_states)
        if not consensus_state:
            # If there is no consensus, choose one of the states arbitrarily
            consensus_state = site_states[0]
        consensus_alignment.append("".join(consensus_state))

    return consensus_alignment



class Node:
    def __init__(self, name, alignment=None):
        self.name = name
        self.alignment = alignment if alignment else [name]
        self.left = None
        self.right = None
        self.distance = None

    def is_leaf(self):
        return self.left is None and self.right is None

    def get_leaf_names(self):
        if self.is_leaf():
            return [self.name]
        else:
            return self.left.get_leaf_names() + self.right.get_leaf_names()

    def get_newick(self):
        if self.is_leaf():
            return self.name
        else:
            return f"({self.left.get_newick()}, {self.right.get_newick()})"

def build_tree_from_distance(distance_matrix, sequence_names):
    n = len(distance_matrix)
    nodes = [Node(name) for name in sequence_names]
    
    while n > 1:
        distance_matrix_shaded = np.empty((n, n))
        for j in range(n):
            for i in range(j, n):  # Используем range(j, n) для затенения верхнего треугольника
                distance_matrix_shaded[i][j] = distance_matrix[i][j]
                distance_matrix_shaded[j][i] = float('inf')
        #print('shaded marix: \n', distance_matrix_shaded)
        # Находим индексы минимального значения в маскированной/треугольной матрице расстояний
        min_i, min_j = np.unravel_index(np.argmin(distance_matrix_shaded), distance_matrix_shaded.shape)
        print('mins: ', min_i, min_j)
        # Создаем новый узел, объедтняющие ближайшие 
        new_node = Node(f"{nodes[min_i].name}_{nodes[min_j].name}", alignment=[])
        # 
        new_node.distance = distance_matrix[min_i, min_j]
        # 
        new_node.left = nodes[min_i]
        new_node.right = nodes[min_j]
        # заменяем старые новым
        nodes[min_j] = new_node
        nodes.pop(min_i)
       # nodes[min_j] = new_node

        print('nodes: ',[node.name for node in nodes] )
        # рассчитываем расстояния от нового узла до остальных
        new_distances = 0.5 * (distance_matrix[min_i, :] + distance_matrix[min_j, :])
        new_distances = np.delete(new_distances, min_i, axis=0)
        print('new distances:', new_distances)

        # перерассчитываем матрицу расстояний
        if min_i < len(new_distances):
            new_distances[min_i] = 0
        
            distance_matrix = np.delete(distance_matrix, min_j, axis=0)
            distance_matrix = np.delete(distance_matrix, min_j, axis=1)
            distance_matrix[min_i, :] = new_distances
            distance_matrix[:, min_i] = new_distances
            print('\nnew matrix: \n', distance_matrix)


        n -= 1
    # Возвращаем корневой узел
    return nodes[0]

def build_tree_from_distance0(distance_matrix, sequence_names):
    n = len(distance_matrix)
    nodes = [Node(name) for name in sequence_names]

    while n > 1:
        distance_matrix_shaded = np.empty((n, n))
        for j in range(n):
            for i in range(j, n):  # Используем range(j, n) для затенения верхнего треугольника
                distance_matrix_shaded[i][j] = distance_matrix[i][j]
                distance_matrix_shaded[j][i] = float('inf')

        # Находим индексы минимального значения в "затененной" матрице расстояний
        min_i, min_j = np.unravel_index(np.argmin(distance_matrix_shaded), distance_matrix_shaded.shape)
        
        new_node = Node(f"{nodes[min_i].name}_{nodes[min_j].name}", alignment=[])

        new_node.distance = distance_matrix[min_i, min_j]

        new_node.left = nodes[min_i]
        new_node.right = nodes[min_j]

        nodes[min_j] = new_node
        nodes.pop(min_i)
        
        new_distances = 0.5 * (distance_matrix[min_i, :] + distance_matrix[min_j, :])
        new_distances = np.delete(new_distances, min_i, axis=0)

        if min_i < len(new_distances):
            new_distances[min_i] = 0
        
            distance_matrix = np.delete(distance_matrix, min_j, axis=0)
            distance_matrix = np.delete(distance_matrix, min_j, axis=1)
            distance_matrix[min_i, :] = new_distances
            distance_matrix[:, min_i] = new_distances

        n -= 1

    return nodes[0]

def progressive_alignment(sequences):
    sequence_names = [f"Seq{i+1}" for i in range(len(sequences))]

    n = len(sequences)
    distance_matrix = np.empty((n, n))
    # distance_matrix = np.array([[0.0, 0.88, 0.77, 1.0, 0.77], 
    #                             [0.88, 0.0, 0.44, 0.77, 0.77],
    #                             [0.77, 0.44, 0.0, 0.88, 1.0],
    #                             [1.0, 0.77, 0.88, 0.0, 0.77],
    #                             [0.77, 0.77, 1.0, 0.77, 0.0]])
    # print(distance_matrix)
    for i in range(n):
        for j in range(i + 1, n):
            distance_matrix[i][j] = calculate_distance(sequences[i], sequences[j])
            distance_matrix[j][i] = distance_matrix[i][j]

    #print(distance_matrix)

    # lw_dm = distance_matrix.tolist()
    # # for i in range(1, distance_matrix.shape[0]):
    # #     for j in range(i):
    # #         lw_dm.append(distance_matrix[i][j])
    # print(lw_dm)

    # dist_matrix = DistanceMatrix(names=["Seq1", "Seq2", "Seq3", "Seq4", "Seq5"], matrix=lw_dm)
    # distance_calculator = DistanceCalculator('identity')
    # calculator = distance_calculator
    # constructor = DistanceTreeConstructor(calculator)

    # tree = constructor.nj(dist_matrix)
    # final_alignment = tree.clade
    # final_tree = traverse_clade(final_alignment)

    tree = build_tree_from_distance(distance_matrix, sequence_names)

    final_tree = tree.get_newick()
    return final_tree



######################################################################################

def traverse_clade(clade):
    if clade.is_terminal():
        return clade.name
    else:
        left_subtree = traverse_clade(clade.clades[0])
        right_subtree = traverse_clade(clade.clades[1])
        return f"({left_subtree},{right_subtree})"

# Выравнивание с помощью Bio
def progressive_alignment_bio(sequences):
    distance_calculator = DistanceCalculator('identity')
    calculator = distance_calculator
    alignment = MultipleSeqAlignment(sequences)
    dm = calculator.get_distance(alignment)
    print(dm)
    constructor = DistanceTreeConstructor(calculator)
    tree = constructor.nj(dm)
    final_alignment = tree.clade
    alignment_tree = traverse_clade(final_alignment)
    print('tree', tree)
    print('clade', final_alignment)
    print('alignment tree', alignment_tree)
    return alignment_tree




# Загрузка последовательностей из файла
file_path = 'sequences.fasta'
sequences = read_sequences_from_fasta(file_path)

final_tree_bio = progressive_alignment_bio(sequences)

print("\nРезультат с использованием методов библиотеки Bio:")
print(final_tree_bio)


final_tree = progressive_alignment(sequences)
print('\nРезультат самодельной функции: ')
print(final_tree)



