import numpy as np
from Bio import SeqIO


def load_sequences_from_fasta(file_path):
    sequences = []
    for record in SeqIO.parse(file_path, 'fasta'):
        sequences.append(str(record.seq))
    return sequences

file_path = 'sequences.fasta'
sequences = load_sequences_from_fasta(file_path)[:3]

sequences = ['TAG', 'TTACG', 'TTAGTC', 'CGTCTAGAC', 'GTGAC', 'TATC', 'CGTAGC', 'CTTGA', 'CTAA']
print('sequences: ', sequences)


p = {'A' : {'A' : 0.5, 'G' : 0.05, 'T' : 0.3, 'C' : 0.15},
     'G' : {'A' : 0.05, 'G' : 0.5, 'T' : 0.15, 'C' : 0.3},
     'T' : {'A' : 0.3, 'G' : 0.15, 'T' : 0.5, 'C' : 0.05},
     'C' : {'A' : 0.15, 'G' : 0.3, 'T' : 0.05, 'C' : 0.5}
     }

########################## HMM parameters  ###########################

def f(i, j, p):
    if i == j == 0: return 1
    if i == 0 or j == 0: return 0 #f[i][j] = 0
    else: 
        probs = [p['P_mm'] *f(i-1, j-1, p), \
                 p['P_xm']*fx(i-1, j-1, p), \
                 p['P_ym']*fy(i-1, j-1, p)]
        max_prob = max(probs)
        return p[x[i-1]][y[j-1]]*max_prob

def fx(i, j, p):
    if i == j == 0: return 0
    if i == 0 or j == 0: return 0
    else: return 0.25*max(p['P_mx']*f(i-1, j, p), \
                          p['P_xx']*fx(i-1, j, p))

def fy(i, j, p):
    if i == j == 0: return 0
    if i == 0 or j == 0: return 0
    else: return 0.25*max(p['P_my']*f(i, j-1, p), \
                          p['P_yy']*fy(i, j-1, p))


def trace_back(matrix, x, y):
    path = []
    i, j = len(x), len(y)
    while (i != 0 and j !=0 ):
        #print(j, i)
        probs = matrix[j, i]
                                    
        if np.argmax(probs) == 0: 
            path.append('t')
            i -=1
            j -=1
        if np.argmax(probs) == 1: 
            path.append('d')
            i -=1
        elif np.argmax(probs) == 2: 
            path.append('i')
            j -=1       
    return path     


def alignment(x, y, path):
    al = ['','']
    i = 0
    j = 0
    for k in range(len(path)):
        if path[k] == 't': 
            al[0] += x[i]
            al[1] += y[j]
            i+=1
            j+=1
        elif path[k] == 'd':
            al[0] += x[i]
            al[1] += '_'
            i+=1

        elif path[k] == 'i':
            al[0] += '_'
            al[1] += y[j]
            j+=1

    return al

def estimate_params(alignments):
    # инициализация подсчета встречаемости различных переходов.
    transitions_counts = {'A' : {'A' : 1, 'G' : 1, 'T' : 1, 'C' : 1}, 
                          'G' : {'A' : 1, 'G' : 1, 'T' : 1, 'C' : 1},
                          'T' : {'A' : 1, 'G' : 1, 'T' : 1, 'C' : 1},
                          'C' : {'A' : 1, 'G' : 1, 'T' : 1, 'C' : 1}
                         }
    emission_counts = {'A':1, 'G':1, 'T':1, 'C':1}

    for alignment in alignments:
        for i in range(len(alignment[0])):
            # в зависимости от того, в какой посл-ти встретился пропуск добавляется либо инсерция либо делеция либо транзиция
            if ((alignment[0][i] != '_') and (alignment[1][i] != '_')): transitions_counts[alignment[0][i]][alignment[1][i]] +=1
            elif ((alignment[0][i] == '_') and (alignment[1][i] != '_')): emission_counts[alignment[1][i]] +=1
            elif ((alignment[1][i] == '_') and (alignment[0][i] != '_')): emission_counts[alignment[0][i]] +=1
            else: continue

    transitions = {'A':sum(transitions_counts['A'].values()), 'G':sum(transitions_counts['G'].values()), \
                   'T':sum(transitions_counts['T'].values()), 'C':sum(transitions_counts['C'].values())}

    hmm_trans_params = transitions_counts
    for key1 in hmm_trans_params:
        for key2 in hmm_trans_params[key1]:
            hmm_trans_params[key1][key2] /= transitions[key1]
            hmm_trans_params[key1][key2] = round(hmm_trans_params[key1][key2], 2)
    print('hmm_trans_params: ')#hmm_trans_params
    for x in hmm_trans_params:
        print (x, ': ', hmm_trans_params[x])

    hmm_emis_params = emission_counts
    for key in hmm_emis_params: 
        hmm_emis_params[key] /= sum(emission_counts.values())
        hmm_emis_params[key] = round(hmm_emis_params[key], 2)
    print('hmm_emis_params', hmm_emis_params)

    return [hmm_trans_params, hmm_emis_params]

    
def estimate_op(paths):
    P_mm = 0
    P_xm = 0
    P_ym = 0
    P_mx = 0
    P_my = 0
    P_xx = 0
    P_yy = 0
    m, x, y = 1, 1, 1
    for path in paths:
        m += path.count('t')
        x += path.count('d')
        y += path.count('i')
        for k in range(len(path)-1):
            if path[k] == 't': 
                if (path[k+1] == 't'): P_mm +=1
                elif (path[k+1] == 'd'): P_mx +=1
                elif (path[k+1] == 'i'): P_my +=1

            elif path[k] == 'd': 
                if path[k+1] == 'd': P_xx +=1
                elif path[k+1] == 't': P_xm +=1
            
            elif path[k] == 'i': 
                if path[k+1] == 'i': P_yy +=1
                elif path[k+1] == 't': P_ym +=1

    P_mm /= m
    P_xm /= x
    P_ym /= y
    P_mx /= m
    P_my /= m
    P_xx /= x
    P_yy /= y

    hmm_state_params = {'P_mm': round(P_mm, 2), 'P_xm': round(P_xm, 2), 'P_ym': round(P_ym, 2), 'P_mx': round(P_mx, 2), 'P_my': round(P_my, 2), 'P_xx': round(P_xx, 2), 'P_yy': round(P_yy, 2) }
    hmm_print = str(hmm_state_params)
    print('hmm_state_params: ', hmm_print[:41], '\n                 ', hmm_print[41:])

    return hmm_state_params



p = {'A' : {'A' : 0.25, 'G' : 0.25, 'T' : 0.25, 'C' : 0.25},
     'G' : {'A' : 0.25, 'G' : 0.25, 'T' : 0.25, 'C' : 0.25},
     'T' : {'A' : 0.25, 'G' : 0.25, 'T' : 0.25, 'C' : 0.25},
     'C' : {'A' : 0.25, 'G' : 0.25, 'T' : 0.25, 'C' : 0.25},
     'P_mm': 0.5, 'P_xm': 0.8, 'P_ym': 0.8, 'P_mx': 0.2, 'P_my': 0.2, 'P_xx': 0.1, 'P_yy': 0.1
    }

e = {'A':0.25, 'G':0.25, 'T':0.25, 'C':0.25}

print('init_trans_params: ')
for x in p:
        print (x, ': ', p[x])
print('init_emis_params: ', e, '\n\n')

for iteration in range(6):
    print('iteration ', iteration+1, ':')
    alignments = []
    paths = []
    for m in range(len(sequences)):
        for n in range(m, len(sequences)):
            x = sequences[m]
            y = sequences[n]

            viterbi_matrix = np.zeros((len(y)+1, len(x)+1, 3))
            for i in range(0, len(x)+1):
                for j in range(0, len(y)+1):
                    viterbi_matrix[j, i] = np.array([f(i, j, p), fx(i, j, p), fy(i, j, p)])

            path = trace_back(viterbi_matrix, x, y)
            paths.append(path)
            alignment0 = alignment(x, y, path) 
            alignments.append(alignment0)

    pe = estimate_params(alignments)
    other_params = estimate_op(paths)
    print('\n\n')
    p = dict(list(pe[0].items()) + list(other_params.items()))
    e = pe[1]
    #print(p)
    