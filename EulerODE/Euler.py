# -*- coding: utf-8 -*-

import numpy as np
from scipy.optimize import fsolve
import matplotlib.pyplot as plt



def implicit_euler1(f, y0, t, accuracy = 0.00000000001, h = 0.01):
    # f - функция производной
    # y0 - начальное значение искомой функции
    # t - временной промежуток
    # n - количество итераций в методе Ньютона
    if h is None: h=0.01

    n = int((t[1] - t[0])/h) #len(t)
    m = len(y0)
    #y = []
    y = np.zeros((1, m))
    y[0, :] = y0
    #y.append(y0)
    step = []
    #for i in range(1, n):
    i = 1
    dt = h
    while t[0] + sum(step) < t[1]:
        # Вычисляем линейное приближение искомой функции в следующий момент времени по производной 
        
        y = np.append(y, [(y[i - 1, :]) + dt * f(t[0] + i * dt, y[i - 1, :])], axis=0)
        y_new = y[i-1,:] + dt*(f(t[0] + i*dt, y[i,:]) + f(t[0] + (i-1) * dt, y[i-1,:])) / 2
        #print(np.linalg.norm(y_new - y[i,:]), '   ', accuracy)
        while np.linalg.norm(y_new - y[i,:]) > accuracy:
            dt *= 0.5
            #print(dt)
            y[i,:] = y[i-1,:] + dt * f(t[0] + i * dt, y[i-1, :])
            y_new = y[i-1,:] + dt * (f(t[0] + i * dt, y[i, :]) + f(t[0] + (i-1) * dt, y[i - 1, :])) / 2
            step.append(dt)
        #print(np.linalg.norm(y_new - y[i,:]))
        y[i,:] = y_new
        i+=1    
        step.append(dt)
    
    return y, min(step)



def f(t, y): # Кокретная функция производных 
    dy1_dt = -2*y[0] + y[1] -y[2]
    dy2_dt = y[0] - y[1] + 3*y[2]
    dy3_dt = -y[0] +2*y[1] + y[2]
    return np.array([dy1_dt, dy2_dt, dy3_dt])

# Задаем параметры задачи
y0 = np.array([0, 1, 0])
t_lin = np.linspace(0, 2, 2000)
t = [0, 2]

y, _ = implicit_euler1(f, y0, t, 0.00000001, 0.001)

print(y)

from scipy.integrate import solve_ivp 
solution = solve_ivp(f, [0, 2], y0, t_eval=t_lin)  # Метод для сравнения решений
print('\n True solution: \n', solution)


file = open('results.txt', "w")
#file.write(str(len(A)) + "\n" + str(len(epsilons)))
for eps in y:
    # y, t = implicit_euler1(f, eps, y0, a, b)

    file.write("\n" + str(eps))
    # file.write("\n" + str(len(t)))
    # file.write("\n" + "\n".join(" ".join(map(str, x)) for x in y))
    # file.write("\n" + "\n".join(map(str, t)))

file.close()




# true_accuracy = np.linalg.norm(y - solution.y.T)
# print(true_accuracy)
# steps = [0.2, 0.1, 0.05, 0.02, 0.01, 0.005, 0.002, 0.001, 0.0005, 0.0002, 0.0001] # Набор шагов разной длины
# acc_func = np.linalg.norm(y - solution.y.T) # Вычисление нормы разницы между результатом, полученным функцией реализованной вручную и функцией библиотеки scipy 
# acc = []



# for i in range(11): # Для разных шагов вычисляем решение своим методом и методом scipy
#     true_sol = solve_ivp(f, [0, 1], y0, t_eval=np.linspace(0, 1, int(1/steps[i])))
#     sol, minstep = implicit_euler1(f, y0, t, 0.00001, steps[i])
#     acc.append(np.linalg.norm(sol - true_sol.y.T))

#     y1 = [s[0] for s in y]
#     y2 = [s[1] for s in y]

#     plt.plot(y1, y2, label = 'solution for accuracy = {0}'.format(acc))
#     plt.title('solution for accuracy = {0}'.format(acc[i])) 
#     plt.xlabel('y1')
#     plt.ylabel('y2')
    
# plt.show()

minsteps = []
accuracies = [0.1/(10**n) for n in range(6)]
print("I'm on 113")
for i in range(6): # Для разных допустимых погрешностей вычисляем решение и минимальный шаг
    #true_sol = solve_ivp(f, [0, 1], y0, t_eval=np.linspace(0, 1, int(1/steps[i])))
    sol, minstep = implicit_euler1(f, y0, t, accuracies[i], 0.01)
    print(minstep)

    minsteps.append(minstep)

print(minsteps)
print(accuracies)
plt.plot(accuracies, minsteps)
plt.title('minimal step for every accuracy') 
plt.xlabel('accuracies')
plt.xscale('log')
plt.ylabel('minsteps')
    
plt.show()

# finY1 = []
# finY2 = []
# true_finY = []
# for i in range(11):
#     true_sol = solve_ivp(f, [0, 1], y0, t_eval=np.linspace(0, 1, int(1/steps[i])))
#     sol = implicit_euler1(f, y0, t, 0.00001, steps[i])
#     finY1.append(sol[int(1/steps[i])-1, 0])
#     finY2.append(sol[int(1/steps[i])-1, 1])
#     #true_finY.append(true_sol)

# print(acc)

# plt.plot(steps, acc)
# plt.xscale('log')
# plt.xlabel('length of step')
# plt.ylabel('Error value (linalg.norm)')




# from matplotlib import axes as ax

# #ax.plot(steps, finY1, label = 'y1')
# #ax.plot(steps, finY2, label = 'y2')
# #plt.plot(steps, finY2)
# #plt.xscale('log')
# #plt.xlabel('length of step')
# #plt.ylabel('Y(t)')

# plt.gca().invert_xaxis()
# plt.show()


